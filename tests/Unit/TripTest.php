<?php

namespace Tests\Unit;

use App\Team;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\UploadedFile;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class TripTest extends TestCase {

    use WithFaker;

    /**
     * A basic test example.
     *
     * @return void
     */
    public function test_user_can_create_a_trip_test() {
        $depart_time = Carbon::now()->addMinutes(rand(-525600, 525600))->toDateTimeString();
        $return_time = Carbon::parse($depart_time)->addMinutes(rand(1440, 10080))->toDateTimeString();
        $tripName    = $this->faker->streetName;

        dump($tripName);

        $this->actingAs(User::inRandomOrder()->first());

        $response = $this
            ->followingRedirects()
            ->json('POST',
                                '/trip',
                                [
                                    'team_id'     => Team::inRandomOrder()->first()->id,
                                    'image'       => UploadedFile::fake()->image('pathToImage.png'),
                                    'name'        => $tripName,
                                    'description' => $this->faker->paragraphs(3, true),
                                    'price'       => rand(100, 250),
                                    'depart_at'   => $depart_time,
                                    'return_at'   => $return_time,
                                    'line_1'      => $this->faker->streetAddress,
                                    'line_2'      => $this->faker->secondaryAddress,
                                    'city'        => $this->faker->city,
                                    'state'       => $this->faker->state,
                                    'zip'         => $this->faker->postcode,
                                ])
            ->assertSee($tripName);
    }
}
