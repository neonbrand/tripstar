<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTravelerTripPivotTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('traveler_trip',
            function( Blueprint $table ) {
                $table->integer('traveler_id')->unsigned()->index();
                $table->foreign('traveler_id')->references('id')->on('travelers')->onDelete('cascade');
                $table->integer('trip_id')->unsigned()->index();
                $table->foreign('trip_id')->references('id')->on('trips')->onDelete('cascade');
                $table->primary([ 'traveler_id', 'trip_id' ]);
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('traveler_trip');
    }
}
