<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAddressesTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('addresses',
            function( Blueprint $table ) {
                $table->increments('id');
                $table->morphs('addressable');
                $table->string('name');
                $table->string('line_1');
                $table->string('line_2')->nullable();
                $table->string('city');
                $table->string('state');
                $table->string('zip');
                $table->timestamps();
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('addresses');
    }
}
