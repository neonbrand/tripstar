<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTripsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('trips',
            function( Blueprint $table ) {
                $table->increments('id');
                $table->unsignedInteger('team_id');
                $table->foreign('team_id')->references('id')->on('teams');
                $table->string('image');
                $table->string('name');
                $table->text('description');
                $table->unsignedDecimal('price');
                $table->schemalessAttributes('options');
                $table->dateTime('depart_at');
                $table->dateTime('return_at')->nullable();
                $table->timestamps();
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('trips');
    }
}
