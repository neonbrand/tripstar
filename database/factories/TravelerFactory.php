<?php

use Faker\Generator as Faker;

$factory->define(App\Traveler::class, function (Faker $faker) {
    return [
        'display_name' => $faker->name,
    ];
});

$factory->afterMaking(App\Traveler::class, function ($traveler, $faker) {
    $user = App\User::inRandomOrder()->first();
    if($user === null) {
        factory(App\User::class, 5)->create();
        $user = App\User::inRandomOrder()->first();
    }
    $traveler->user()->associate($user);
});
