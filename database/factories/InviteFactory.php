<?php

use App\Role;
use App\Team;
use App\User;
use Carbon\Carbon;
use Faker\Generator as Faker;
use Illuminate\Support\Facades\DB;

$factory->define(App\Invite::class, function (Faker $faker) {
    $model = array_random([ "Team", "Trip", "Role" ]);
    $id    = DB::table(str_plural($model))->inRandomOrder()->first()->id;
    if($model == "Team") {
        $options = [ 'role' => Role::inRandomOrder()->first()->id ];
    } else if($model == "Role") {
        $options = [ 'team' => Team::inRandomOrder()->first()->id ];
    }
    // else if($model == "Trip") {
    //     $options = [ 'user' => User::inRandomOrder()->first()->id ];
    // }

    return [
        'user_id'        => User::inRandomOrder()->first()->id,
        'email'          => User::inRandomOrder()->first()->email,
        'invitable_type' => "App\\" . $model,
        'invitable_id'   => $id,
        'code'           => $faker->unique()->bothify('******'),
        'options'        => $options ?? [],
        'valid_until'    => Carbon::now()->addMinutes($faker->randomDigit()),
    ];
});