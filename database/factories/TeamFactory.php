<?php

use Faker\Generator as Faker;

$factory->define(App\Team::class, function (Faker $faker) {
    return [
        'display_name' => $faker->company,
        'description'  => $faker->catchPhrase,
        'logo'         => 'pathToLogo.png',
        'theme'        => [ 'red', 'orange', 'yellow', 'green', 'blue', 'indigo', 'violet', 'white', 'black' ][ rand(0, 8) ]
    ];
});

$factory->afterMaking(App\Team::class, function ($team, $faker) {
    $address = factory(App\Address::class)->make();
    $team->push();
    $address->addressable()->associate($team);
    $address->push();
});
