<?php

use Carbon\Carbon;
use Faker\Generator as Faker;

$factory->define(App\Trip::class, function (Faker $faker) {
    $depart_time = Carbon::now()->addMinutes(rand(-525600, 525600));
    $return_time = Carbon::parse($depart_time)->addMinutes(rand(1440, 10080));

    return [
        'name'        => 'Trip to a place',
        'image'       => 'pathToImage.png',
        'description' => $faker->paragraph(20, true),
        'price'       => (rand(1000, 20000) / 100),
        'options'     => [ 'key' => 'value' ],
        'depart_at'   => $depart_time,
        'return_at'   => $return_time,
    ];
});

$factory->afterMaking(App\Trip::class, function ($trip, $faker) {
    $team = App\Team::inRandomOrder()->first();
    $address = factory(App\Address::class)->make();
    $trip->team()->associate($team);
    $trip->push();
    $address->addressable()->associate($trip);
    $address->push();
});