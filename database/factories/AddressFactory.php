<?php

use Faker\Generator as Faker;

$factory->define(App\Address::class, function (Faker $faker) {
    return [
        'name'   => array_random([ 'Home', 'Work', 'Hotel' ]),
        'line_1' => $faker->streetAddress,
        'line_2' => array_random([ "", $faker->buildingNumber ]),
        'city'   => $faker->city,
        'state'  => $faker->stateAbbr,
        'zip'    => $faker->postcode,
    ];
});