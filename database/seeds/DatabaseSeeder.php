<?php

use App\Invite;
use App\Role;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder {

    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run() {
        $this->command->info('Creating Roles...');
        Role::create([
                         'name'         => 'Owner',
                         'display_name' => 'owner',
                         'description'  => 'Allows every permission ever for this team.',
                     ]);

        Role::create([
                         'name'         => 'Admin',
                         'display_name' => 'admin',
                         'description'  => 'Allows most permissions for this team.',
                     ]);

        Role::create([
                         'name'         => 'Moderator',
                         'display_name' => 'moderator',
                         'description'  => 'Allows for invites for this team.',
                     ]);
        $this->command->info('Creating Teams...');
        factory(App\Team::class, 10)->create();
        $this->command->info('Creating Users...');
        factory(App\User::class, 10)->create();
        $this->command->info('Creating Travelers...');
        factory(App\Traveler::class, 100)->create();
        $this->command->info('Creating Trips...');
        factory(App\Trip::class, 200)->create();
        $this->command->info('Creating Invites...');
        factory(App\Invite::class, 400)->create();
        $this->command->info('Consuming all invites...');

        foreach(Invite::get() as $invite) {
            App\Jobs\ConsumeInvite::dispatch($invite);
        }

        $this->command->info('Creating 100 more Invites...');
        factory(App\Invite::class, 400)->create();
    }
}
