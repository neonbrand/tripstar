<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/home');
});

Auth::routes(['verify' => true]);

Route::group(['middleware' => ['auth']], function () {
    Route::get('/home', 'HomeController@index')->name('home');

    Route::resource('/trip', 'TripController')->except(['show', 'index']);
    Route::group(['middleware' => ['verified']], function () {
        Route::resource('/team', 'TeamController');
    });
});

// Make trips visible to the public
Route::get('/trip/{trip}', 'TripController@show');
    // Just testing this middleware, checks if the user has a traveler assigned to the trip.
    //  ->middleware('can:view,trip');