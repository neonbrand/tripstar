<?php

namespace App;

use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;
use Laratrust\Models\LaratrustRole;

class Role extends LaratrustRole {

    use HasSlug;
    protected $guarded = [ 'id' ];

    /**
     * Get the options for generating the slug.
     */
    public function getSlugOptions() : SlugOptions {
        return SlugOptions::create()
                          ->generateSlugsFrom('display_name')
                          ->saveSlugsTo('name');
    }

    public function invites() {
        return $this->morphMany(Invite::class, 'invitable');
    }
}
