<?php

namespace App\Http\Controllers;

use App\Invite;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        $future_trips = collect();
        $past_trips = collect();
        foreach($user->travelers as $traveler) {
            foreach($traveler->trips()->future()->orderBy('depart_at', 'asc')->get() as $trip) {
                $future_trips->push($trip);
            }
            foreach($traveler->trips()->past()->orderBy('depart_at', 'desc')->get() as $trip) {
                $past_trips->push($trip);
            }
        }
        $invites = Invite::where('email', $user->email)->get();

        return view('home', compact('future_trips', 'past_trips', 'invites'));
    }
}
