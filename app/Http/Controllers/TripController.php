<?php

namespace App\Http\Controllers;

use App\Address;
use App\Trip;
use Carbon\Carbon;
use Illuminate\Http\Request;

class TripController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        return view('trips.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store( Request $request ) {
        $validatedTrip    = $request->validate([
                                                   // TODO allow the teams that they belong to for Team ID
                                                   'team_id'     => 'required',
                                                   'image'       => 'required|image',
                                                   'name'        => 'required|max:255',
                                                   'description' => 'string|max:65500',
                                                   'price'       => 'integer|max:65000',
                                                   'depart_at'   => 'required|before:return_at',
                                                   'return_at'   => 'nullable|after:depart_at',
                                               ]);

        $validatedAddress = $request->validate([
                                                   'line_1'  => 'required|string|max:255',
                                                   'line_2' => 'string|max:255',
                                                   'city'     => 'required|string|max:255',
                                                   'state'    => 'required|string|max:255',
                                                   'zip'      => 'required|string',
                                               ]);

        if($request->hasFile('image') && $request->file('image')->isValid()) {
            $path                 = $request->image->store('images', 'public');
            $validatedTrip[ 'image' ] = $path;
        }

        // Make the name of the address the trip name
        $validatedAddress['name'] = $validatedTrip['name'];
        $trip = Trip::create($validatedTrip);

        $validatedAddress['addressable_type'] = get_class($trip);
        $validatedAddress['addressable_id'] = $trip->id;
        Address::create($validatedAddress);

        return redirect("/trip/{$trip->slug}");

    }

    /**
     * Display the specified resource.
     *
     * @param \App\Trip $trip
     *
     * @return \Illuminate\Http\Response
     */
    public function show( Trip $trip ) {
        return view('trips.show', compact('trip'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit( $id ) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int                      $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update( Request $request, $id ) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy( $id ) {
        //
    }
}
