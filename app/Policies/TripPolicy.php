<?php

namespace App\Policies;

use App\User;
use App\Trip;
use Illuminate\Auth\Access\HandlesAuthorization;

class TripPolicy {

    use HandlesAuthorization;

    /**
     * Determine whether the user can view the trip.
     *
     * @param  \App\User $user
     * @param  \App\Trip $trip
     *
     * @return mixed
     */
    public function view( User $user, Trip $trip ) {
        // $user->load([ 'travelers.trips' => function( $q ) use ( &$trips ) {
        //     $trips = $q->get()->unique();
        // } ]);
        //
        // if($trips->contains('id', $trip->id)) {
            return true;
        // } else {
        //     return false;
        // }
    }

    /**
     * Determine whether the user can create trips.
     *
     * @param  \App\User $user
     *
     * @return mixed
     */
    public function create( User $user ) {
        return true;
    }

    /**
     * Determine whether the user can update the trip.
     *
     * @param  \App\User $user
     * @param  \App\Trip $trip
     *
     * @return mixed
     */
    public function update( User $user, Trip $trip ) {
        return true;
    }

    /**
     * Determine whether the user can delete the trip.
     *
     * @param  \App\User $user
     * @param  \App\Trip $trip
     *
     * @return mixed
     */
    public function delete( User $user, Trip $trip ) {
        return true;
    }

    /**
     * Determine whether the user can restore the trip.
     *
     * @param  \App\User $user
     * @param  \App\Trip $trip
     *
     * @return mixed
     */
    public function restore( User $user, Trip $trip ) {
        return true;
    }

    /**
     * Determine whether the user can permanently delete the trip.
     *
     * @param  \App\User $user
     * @param  \App\Trip $trip
     *
     * @return mixed
     */
    public function forceDelete( User $user, Trip $trip ) {
        return true;
    }
}
