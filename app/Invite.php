<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Spatie\SchemalessAttributes\SchemalessAttributes;

class Invite extends Model {

    protected $guarded = [ 'id' ];

    protected $casts = [
        'options' => 'array',
    ];

    public function getOptionsAttribute() : SchemalessAttributes {
        return SchemalessAttributes::createForModel($this, 'options');
    }

    public function scopeWithOptions() : Builder {
        return SchemalessAttributes::scopeWithSchemalessAttributes('options');
    }

    public function invitable() {
        return $this->morphTo();
    }

    public function inviter() {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function invitee() {
        return $this->belongsTo(User::class, 'email', 'email');
    }
}
