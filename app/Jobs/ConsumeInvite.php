<?php

namespace App\Jobs;

use App\Invite;
use App\Role;
use App\Team;
use App\Traveler;
use App\Trip;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Foundation\Bus\Dispatchable;

class ConsumeInvite {

    use Dispatchable, Queueable;

    protected $invite;

    protected $traveler;

    /**
     * Create a new job instance.
     *
     * @param \App\Invite   $invite
     * @param \App\Traveler $traveler
     */
    public function __construct( Invite $invite, Traveler $traveler = null ) {
        $this->invite   = $invite;
        $this->traveler = $traveler;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle() {
        $traveler  = $this->traveler;
        $invite    = $this->invite;
        $invitable = $invite->invitable;
        $invitee   = $invite->invitee;


        // TODO check whether the invitee or the inviter exist prior to continuing

        // Handle the fact that you may get sent an invite without a traveler.
        // In this case, use the first traveler from the user that matches the email.
        // If you get an invite for a user without a traveler, create a traveler based
        // on the User information

        try {
            if($traveler == null) {
                $traveler = $invitee->travelers()->first();
            }

            if($traveler == null) {
                $traveler = Traveler::create([
                                                 'user_id'      => $invitee->id,
                                                 'display_name' => $invitee->name,
                                             ]);
            }
        } catch( \Exception $e ) {
            logger('Traveler not found for user id ' . $this->invite->options->user);
        }

        switch(true) {
            case $invitable instanceof Trip:
                $traveler->trips()->syncWithoutDetaching($invitable);
                $invite->delete();
                break;
            case $invitable instanceof Team:
                $roleId = $invite->options->role;
                $invitee->syncRolesWithoutDetaching([ $roleId ], $invitable);
                $invite->delete();
                break;
            case $invitable instanceof Role:
                $teamId = $invite->options->team;
                $invitee->syncRolesWithoutDetaching([ $invitable ], $teamId);
                $invite->delete();
                break;
        }
    }
}
