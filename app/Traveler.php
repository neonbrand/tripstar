<?php

namespace App;

use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;
use Illuminate\Database\Eloquent\Model;

class Traveler extends Model {

    use HasSlug;

    protected $guarded = [ 'id' ];

    protected $casts = [
        'options' => 'array',
    ];

    /**
     * Get the options for generating the slug.
     */
    public function getSlugOptions() : SlugOptions {
        return SlugOptions::create()
                          ->generateSlugsFrom('display_name')
                          ->saveSlugsTo('name');
    }

    public function user() {
        return $this->belongsTo(User::class);
    }

    public function trips() {
        return $this->belongsToMany(Trip::class);
    }
}