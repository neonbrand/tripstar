<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Address extends Model {

    protected $guarded = [ 'id' ];

    public function addressable() {
        return $this->morphTo();
    }

    public function getOneLineAttribute() {
        return $this->line_1 . ', ' . ($this->line_2 ? $this->line_2 . ', ' : '') . $this->state . ' ' . $this->zip;
    }
}
