<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Spatie\SchemalessAttributes\SchemalessAttributes;
use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;
use Illuminate\Database\Eloquent\Model;

class Trip extends Model {

    use HasSlug;

    protected $guarded = [ 'id' ];

    protected $casts = [
        'options' => 'array',
        'depart_at' => 'datetime',
        'return_at' => 'datetime'
    ];

    protected $dates = [
        'created_at',
        'updated_at',
        'depart_at',
        'return_at',
    ];

    public function setDepartAtAttribute($value) {
        $this->attributes['depart_at'] = Carbon::parse($value)->toDateTimeString();
    }

    public function setReturnAtAttribute($value) {
        $this->attributes['depart_at'] = Carbon::parse($value)->toDateTimeString();
    }

    public function getSlugOptions() : SlugOptions {
        return SlugOptions::create()
                          ->generateSlugsFrom('name')
                          ->saveSlugsTo('slug');
    }

    public function getExtraAttributesAttribute() : SchemalessAttributes {
        return SchemalessAttributes::createForModel($this, 'options');
    }

    public function scopeWithExtraAttributes() : Builder {
        return SchemalessAttributes::scopeWithSchemalessAttributes('options');
    }

    public function address() {
        return $this->morphOne(Address::class, 'addressable');
    }

    public function team() {
        return $this->belongsTo(Team::class);
    }

    public function invites() {
        return $this->morphMany(Invite::class, 'invitable');
    }

    public function travelers() {
        return $this->belongsToMany(Traveler::class);
    }

    public function scopeFuture($query) {
        return $query->where('depart_at', '>', Carbon::now());
    }

    public function scopePast( $query ) {
        return $query->where('return_at', '<', Carbon::now());
    }

    public function scopeCurrent( $query ) {
        return $query
            ->where('depart_at', '<', Carbon::now())
            ->where('return_at', '>', Carbon::now());
    }

    public function invitedTravelers(  ) {
        return $this->morphMany(Invite::class, 'invitable');
    }

    /**
     * Get the route key for the model.
     *
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'slug';
    }
}
