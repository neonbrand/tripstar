@extends('layouts.app')

@section('content')
    <div class="flex flex-col flex-grow font-roboto">
        @component('partials.hero')
            @slot('line_2') {{ auth()->user()->name }} @endslot
        @endcomponent
        <div class="flex-grow py-5 container mx-auto max-w-sm">
            <div class="flex flex-row justify-center content-start space-between items-start">
                <button id="past-trips" class="pill">
                    <span class="md:hidden">@svg('solid/map-marked-alt')</span>
                    <span class="hidden md:inline-block">Past Trips</span>
                </button>
                <button id="invites" class="pill @if(!$invites->isEmpty()) active @endif">
                    @if($invites->isEmpty())
                        <span class="md:hidden">@svg('solid/envelope')</span>
                    @else
                        <span class="md:hidden">@svg('solid/envelope-open-text')</span>
                    @endif
                    <span class="hidden md:inline-block">Invites</span>
                    ({{ auth()->user()->invites->count() }})
                </button>
                <button id="upcoming-trips" class="pill @if($invites->isEmpty()) active @endif">
                    <span class="md:hidden">@svg('solid/plane')</span>
                    <span class="hidden md:inline-block">Upcoming Trips</span>
                </button>
            </div>
            <div id="upcoming-trips-area"
                 class="@if($invites->isEmpty()) flex @else hidden @endif area flex-col justify-start items-center space-between items-start m-2">
                @if(!$future_trips->isEmpty())
                    @foreach($future_trips as $trip)
                        @component('partials.dashboard.trip', compact('trip'))
                        @endcomponent
                    @endforeach
                @endif
            </div>
            <div id="past-trips-area" class="hidden area flex-col justify-start items-center space-between items-start m-2">
                @if(!$past_trips->isEmpty())
                    @foreach($past_trips as $trip)
                        @component('partials.dashboard.trip', compact('trip'))
                        @endcomponent
                    @endforeach
                @endif
            </div>
            <div id="invites-area"
                 class="@if($invites->isEmpty()) hidden @else flex @endif area flex-col justify-start items-center space-between items-start">
                @if(!$invites->isEmpty())
                    @foreach($invites as $invite)
                        @php
                            $invitable = $invite->invitable;
                            switch(true) {
                                case $invitable instanceof App\Trip:
                                    $text = "You've been invited to go on a trip!";
                                    break;
                                case $invitable instanceof App\Team:
                                    $text = "You've been invited to manage a team!";
                                    break;
                                case $invitable instanceof App\Role:
                                    $text = "You've been invited to manage a team!";
                                    break;
                            }
                        @endphp
                    <div class="w-full p-2 flex flex-row justify-between content-between between">
                        <div>{{ $text }}</div>
                        <div>Yes/No</div>
                    </div>
                    @endforeach
                @endif
            </div>
        </div>

    </div>
@endsection

@section('scripts')
    <script>
        $('.pill').click(function () {
            $('.pill').removeClass('active');
            $(this).addClass('active');
            $('.area').removeClass('flex').addClass('hidden');
            $('#' + $(this).attr('id') + '-area').removeClass('hidden').addClass('flex');
        });
    </script>
@endsection

