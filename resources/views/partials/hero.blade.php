<div class="flex-none w-100">
    <div
            class="h-{{ $height ?? 64 }} p-4 flex-grow flex flex-col justify-end content-end items-start"
            style="
                    background: url('{{ asset('images/liam-pozz-455682-unsplash.jpg') }}') no-repeat center center fixed;
                    background-size: cover;
                    "
    >
        <div class="container mx-auto max-w-md">
            <div class="text-white font-raleway font-hairline text-xl">{{ $line_1 ?? 'Hi There!' }}</div>
            <div class="text-white font-raleway font-normal text-3xl">{{ $line_2 ?? 'Welcome To TripStar' }}</div>
        </div>
    </div>
</div>