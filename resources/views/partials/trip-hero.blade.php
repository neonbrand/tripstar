<div class="flex-none w-100">
    <div
            class="h-{{ $height ?? 64 }} p-4 flex-grow flex flex-col justify-end content-end items-start"
            style="background: linear-gradient(to bottom, rgba(0,0,0,0.2), rgba(0,0,0,0.4)), url('{{ asset("images/{$trip->image}") }}') no-repeat center center;
                    background-size: cover;"
    >
        <div class="h-full container mx-auto max-w-md">
            <div class="h-full flex flex-col justify-between">
                <div>
                    <img class="h-16" src="{{ asset("images/{$trip->team->logo}") }}" alt="">
                    <div class="text-white font-raleway font-normal text-xl">{{ $trip->team->display_name }}</div>
                </div>
                <div>
                    <div class="text-white font-raleway font-normal text-2xl">{{ $trip->name }}</div>
                    <div class="text-white font-raleway font-hairline text-lg">{{ $trip->address->one_line ?? "Add an address!" }}</div>
                </div>
            </div>
        </div>
    </div>
</div>