<a href="{{ action('TripController@show', ['trip' => $trip]) }}" class="w-full hover:shadow m-2 p-1">
    <div class="w-full flex flex-row justify-between content-between between">
        <div class="flex flex-col justify-between">
            <div class="text-blue">Trip Name</div>
            <div>{{ $trip->name }}</div>
        </div>
        <div class="flex flex-col justify-between">
            <div class="text-blue">Departure</div>
            <div>{{ $trip->depart_at->diffForHumans() }}</div>
            <div class="text-xs">{{ $trip->depart_at->format('M d g:ia') }}</div>
        </div>
        <div class="flex items-center justify-center content-center">
            <span class="text-orange hover:text-orange-light">
                @svg('regular/arrow-alt-circle-right')
            </span>
        </div>
    </div>
</a>