<nav class="flex-none bg-blue py-6 px-3">
    <div class="container flex items-center justify-between flex-wrap mx-auto m-w-lg">
        <a href="{{ url('/') }}">
            <div class="flex items-center flex-no-shrink text-white mr-6">
                @svg('solid/jedi', ['class' => 'icon fill-current h-8 w-8 mr-2'])
                <span class="text-white font-semibold text-xl tracking-tight">TripStar</span>
            </div>
        </a>
        @guest
        @if(Route::is('login'))
            <div>
                <a href="{{ route('register') }}"
                   class="no-underline inline-block text-sm px-4 py-2 leading-none border rounded text-white border-white hover:border-transparent hover:text-blue hover:bg-white lg:mt-0">Register</a>
            </div>
        @else
            <div>
                <a href="{{ route('login') }}"
                   class="no-underline inline-block text-sm px-4 py-2 leading-none border rounded text-white border-white hover:border-transparent hover:text-blue hover:bg-white lg:mt-0">Login</a>
            </div>
        @endif
        @endguest
        @auth
        <div class="block lg:hidden">
            <button id="toggleNav"
                    class="flex items-center px-3 py-2 border rounded text-blue-lightest border-blue-lightest hover:text-white hover:border-white">
                @svg('solid/bars', ['class' => 'icon fill-current h-3 w-3'])
            </button>
        </div>
        <div id="expand" class="w-full block flex-grow lg:flex lg:items-center lg:w-auto hidden">
            <div class="text-sm lg:flex-grow">
                <a href="{{ action('HomeController@index') }}"
                   class="navbar-link lg:inline-block lg:mt-0">
                    Dashboard
                </a>
                <a href="{{ action('TripController@create') }}"
                   class="navbar-link lg:inline-block lg:mt-0">
                    New Trip
                </a>
            </div>
            <div>
                <form action="{{ route('logout') }}" method="POST">
                    @csrf
                    <button class="no-underline inline-block text-sm px-4 py-2 leading-none border rounded text-white border-white hover:border-transparent hover:text-blue hover:bg-white mt-4 lg:mt-0">
                        Logout
                    </button>
                </form>
            </div>
        </div>
        @endauth
    </div>
</nav>

@section('scripts')
    <script>
        $('#toggleNav').click(function () {
            $('#expand').toggleClass('hidden');
        });
    </script>
@append
