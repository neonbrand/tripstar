@extends('layouts.app')

@section('content')
    <div class="flex flex-col flex-grow font-roboto">
        @component('partials.hero')
        @endcomponent
        <div class="flex-grow flex flex-col items-center py-10 px-2">
            <div class="w-full max-w-sm" style="min-width:250px">
                <form method="POST" action="{{ route('login') }}">
                    @csrf
                    <div class="mb-4">
                        <label class="block text-grey-darker text-sm font-bold mb-2" for="email">
                            Your Email
                        </label>
                        <input  autofocus
                                value="{{ old('email') }}"
                                name="email"
                                class="input @if(count($errors->get('email'))) border-red @endif"
                                id="email"
                                type="email"
                                placeholder="your@email.here">
                        @if(count($errors->get('email')))
                            @foreach($errors->get('email') as $message)
                                <p class="text-red text-xs italic">{{ $message }}</p>
                            @endforeach
                        @endif
                    </div>
                    <div class="mb-6">
                        <label class="block text-grey-darker text-sm font-bold mb-2" for="password">
                            Password
                        </label>
                        <input
                                name="password"
                                class="input @if(count($errors->get('password'))) border-red @endif"
                                id="password"
                                type="password"
                                placeholder="******************">
                        @if(count($errors->get('password')))
                            @foreach($errors->get('password') as $message)
                                <p class="text-red text-xs italic">{{ $message }}</p>
                            @endforeach
                        @endif
                    </div>
                    <div class="flex items-center justify-between">
                        <button class="btn-blue">
                            Login
                        </button>
                        <a href="{{ route('password.request') }}" class="no-underline inline-block align-baseline font-bold text-xs text-blue hover:text-blue-darker">
                            Forgot Password?
                        </a>
                    </div>
                </form>
            </div>
        </div>
        <p class="text-center text-grey text-xs">
            ©2018 NeONBRAND All rights reserved.
        </p>
    </div>
@endsection