@extends('layouts.app')

@section('content')
    <div class="flex flex-col flex-grow font-roboto">
        @component('partials.hero')
        @endcomponent
        <div class="flex-grow flex flex-col items-center py-10 px-2">
            @if (session('status'))
                <div class="bg-grey-lightest border border-grey-light text-grey-dark text-sm px-4 py-3 rounded mb-4">
                    {{ session('status') }}
                </div>
            @endif
            <div class="w-full max-w-sm" style="min-width:250px">
                <form method="POST" action="{{ route('password.email') }}">
                    @csrf
                    <div class="mb-4">
                        <label class="block text-grey-darker text-sm font-bold mb-2" for="email">
                            Your Email
                        </label>
                        <input  autofocus
                                value="{{ old('email') }}"
                                name="email"
                                class="input @if(count($errors->get('email'))) border-red @endif"
                                id="email"
                                type="email"
                                placeholder="your@email.here">
                        @if(count($errors->get('email')))
                            @foreach($errors->get('email') as $message)
                                <p class="text-red text-xs italic">{{ $message }}</p>
                            @endforeach
                        @endif
                    </div>
                    <div class="flex items-center justify-between">
                        <button class="btn-blue">
                            Send password reset link
                        </button>
                    </div>
                </form>
            </div>
        </div>
        <p class="text-center text-grey text-xs">
            ©2018 NeONBRAND All rights reserved.
        </p>
    </div>
@endsection