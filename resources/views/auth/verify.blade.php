@extends('layouts.app')

@section('content')
    @extends('layouts.app')

@section('content')
    <div class="flex flex-col flex-grow font-roboto">
        @component('partials.hero')
            @slot('line_1') Verify your @endslot
            @slot('line_2') Email Address @endslot
        @endcomponent
        <div class="flex-grow flex flex-col items-center py-10 px-2">
            <div class="w-full max-w-sm" style="min-width:250px">
                <div class="bg-white px-8 py-6 rounded-b">
                    @if (session('resent'))
                        <div class="bg-grey-lightest border border-grey-light text-grey-dark text-sm px-4 py-3 rounded mb-4">
                            {{ __('A fresh verification link has been sent to your email address.') }}
                        </div>
                    @endif

                    <p class="text-grey-dark">
                        {{ __('Before proceeding, please check your email for a verification link.') }}
                        {{ __('If you did not receive the email') }}, <a href="{{ route('verification.resend') }}" class="no-underline hover:underline text-blue">{{ __('click here to request another') }}</a>.
                    </p>
                </div>
            </div>
        </div>
        <p class="py-2 text-center text-grey text-xs">
            ©2018 NeONBRAND All rights reserved.
        </p>
    </div>
@endsection
