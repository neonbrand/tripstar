@php
    setlocale(LC_MONETARY, 'en_US.UTF-8');
@endphp

@extends('layouts.app')

@section('content')
    <div class="flex text-grey-darker flex-col flex-grow font-roboto">
        @component('partials.trip-hero', compact('trip'))
        @endcomponent
        <div class="flex justify-center py-6 px-2 bg-blue">
            <button class="btn btn-blue shadow-lg mr-3">Pay Now</button>
            <button class="btn btn-blue text-grey-light">Payment Options</button>
        </div>
        <div class="bg-grey-lighter border-b border-grey">
            <div class="w-full flex justify-between py-6 px-2 container mx-auto max-w-sm">
                <div class="flex flex-col justify-between">
                    <div class="text-blue text-sm p-1">Trip Name</div>
                    <div class="p-1">{{ $trip->name }}</div>
                </div>
                <div class="flex flex-col justify-between">
                    <div class="text-blue text-sm p-1">Travelers</div>
                    <div class="p-1">{{ $trip->travelers()->count() }}</div>
                </div>
                <div class="flex flex-col justify-between">
                    <div class="text-blue text-sm p-1">Departure</div>
                    <div class="p-1">{{ $trip->depart_at->format('M d g:ia') }}</div>
                </div>
            </div>
        </div>
        <div class="flex-grow">
            <div class="w-full flex flex-col py-6 px-2 justify-between container mx-auto max-w-sm">
                <div class="flex flex-col justify-between pb-2">
                    <div class="text-blue text-sm p-1">Description</div>
                    <div class="p-1">{{ $trip->description }}</div>
                </div>
                <div class="flex flex-col justify-between pb-2">
                    <div class="text-blue text-sm p-1">Trip Pricing</div>
                    <div class="p-1">{{ money_format('%.2n', $trip->price) }}</div>
                </div>
                @if($trip->travelers()->count() > 0)
                    <div class="flex flex-col justify-between pb-2">
                        <div class="text-blue text-sm p-1">Travelers</div>
                        <div class="p-1">
                            @foreach($trip->travelers as $traveler)
                                @if($loop->last)
                                    {{ $traveler->display_name }}
                                @else
                                    {{ $traveler->display_name }},
                                @endif
                            @endforeach
                        </div>
                    </div>
                @endif
                @if($trip->invitedTravelers()->count() > 0 && !Auth::guest())
                    <div class="flex flex-col justify-between pb-2">
                        <div class="text-blue text-sm p-1">Invited</div>
                        <div class="p-1">
                            @foreach($trip->invitedTravelers as $invite)
                                @if($loop->last)
                                    {{ $invite->email }}
                                @else
                                    {{ $invite->email }},
                                @endif
                            @endforeach
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </div>
@endsection