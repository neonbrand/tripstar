@extends('layouts.app')

@section('content')
    <div class="flex text-grey-darker flex-col flex-grow font-roboto">
        @component('partials.hero')
            @slot('height') 64 @endslot
            @slot('line_2') Create an Event @endslot
        @endcomponent
        <div class="p-3">
            <form enctype="multipart/form-data" action="{{ route('trip.store') }}" method="POST" class="w-full max-w-md container mx-auto">
                @csrf
                @include('trips.fields')
                <button class="btn btn-blue">Save</button>
            </form>
        </div>
    </div>
@endsection