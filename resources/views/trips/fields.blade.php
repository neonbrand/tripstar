<div class="flex flex-wrap -mx-3 mb-6">
    <div class="w-full px-3 mb-6 md:mb-0">
        <label class="block uppercase tracking-wide text-grey-darker text-xs font-bold mb-2" for="name">
            Trip Name
        </label>
        <input autofocus
               value="{{ old('name') }}"
               name="name"
               class="input @if(count($errors->get('name'))) border-red @endif"
               id="name"
               type="text"
               placeholder="Your name here...">
        @if(count($errors->get('name')))
            @foreach($errors->get('name') as $message)
                <p class="text-red text-xs italic">{{ $message }}</p>
            @endforeach
        @endif
    </div>
    <div class="w-full md:w-1/2 px-3 mb-6 md:mb-0">
        <label class="block uppercase tracking-wide text-grey-darker text-xs font-bold mb-2" for="team_id">
            Your Team
        </label>
        <div class="relative">
            <select name="team_id"
                    class="block appearance-none w-full bg-grey-lighter border border-grey-lighter text-grey-darker py-3 px-4 pr-8 rounded leading-tight focus:outline-none focus:bg-white focus:border-grey @if(count($errors->get('team'))) border-red @endif" id="team">
                @foreach(\App\Team::get() as $team)
                    <option value="{{ $team->id }}">{{ $team->display_name }}</option>
                @endforeach
            </select>
            <div class="pointer-events-none absolute pin-y pin-r flex items-center px-2 text-grey-darker">
                @svg('solid/angle-down', ['class' => 'fill-current h-4 w-4'])
            </div>
        </div>
        @if(count($errors->get('price')))
            @foreach($errors->get('price') as $message)
                <p class="text-red text-xs italic">{{ $message }}</p>
            @endforeach
        @endif
    </div>
    <div class="w-full md:w-1/2 px-3 mb-6 md:mb-0">
        <label class="block text-grey-darker text-sm font-bold mb-2" for="price">
            Trip Price
        </label>
        <input  value="{{ old('price') }}"
                name="price"
                class="input @if(count($errors->get('price'))) border-red @endif"
                id="price"
                type="number" step=".01"
                placeholder="Your price here...">
        @if(count($errors->get('price')))
            @foreach($errors->get('price') as $message)
                <p class="text-red text-xs italic">{{ $message }}</p>
            @endforeach
        @endif
    </div>
    <div class="w-full px-3 mb-6 md:mb-0">
        <label class="block text-grey-darker text-sm font-bold mb-2" for="description">
            Your Description
        </label>
        <textarea  name="description"
                   class="input @if(count($errors->get('description'))) border-red @endif"
                   id="description"
                   type="text"
                   rows="5"
                   placeholder="Your description here...">{{ old('description') }}</textarea>
        @if(count($errors->get('description')))
            @foreach($errors->get('description') as $message)
                <p class="text-red text-xs italic">{{ $message }}</p>
            @endforeach
        @endif
    </div>
    <div class="w-full md:w-1/2 px-3 mb-6 md:mb-0">
        <label class="block text-grey-darker text-sm font-bold mb-2" for="depart_at">
            Your Trip's Departure Time
        </label>
        <input  value="{{ old('depart_at') }}"
                name="depart_at"
                class="input @if(count($errors->get('depart_at'))) border-red @endif"
                id="depart_at"
                type="text"
                placeholder="Your departure time here...">
        @if(count($errors->get('depart_at')))
            @foreach($errors->get('depart_at') as $message)
                <p class="text-red text-xs italic">{{ $message }}</p>
            @endforeach
        @endif
    </div>
    <div class="w-full md:w-1/2 px-3 mb-6 md:mb-0">
        <label class="block text-grey-darker text-sm font-bold mb-2" for="return_at">
            Your Trip's Return Time
        </label>
        <input  value="{{ old('return_at') }}"
                name="return_at"
                class="input @if(count($errors->get('return_at'))) border-red @endif"
                id="return_at"
                type="text"
                placeholder="Your return time here...">
        @if(count($errors->get('return_at')))
            @foreach($errors->get('return_at') as $message)
                <p class="text-red text-xs italic">{{ $message }}</p>
            @endforeach
        @endif
    </div>
    <div class="w-full px-3 my-6">
        <label class="w-full flex flex-col items-center px-4 py-6 bg-white text-blue rounded-lg shadow-lg tracking-wide uppercase border @if(count($errors->get('image'))) border-red @else border-blue @endif cursor-pointer hover:bg-blue hover:text-white">
            @svg('solid/cloud-upload-alt')
            <span class="mt-2 text-base leading-normal">Select Trip Image</span>
            <input name="image" type='file' class="hidden" />
        </label>
        @if(count($errors->get('image')))
            @foreach($errors->get('image') as $message)
                <p class="text-red text-xs italic pt-3">{{ $message }}</p>
            @endforeach
        @endif
    </div>
    <div class="w-full px-3 mb-6 md:mb-0">
        <label class="block text-grey-darker text-sm font-bold mb-2" for="line_1">
            Your Destination
        </label>
        <input  value="{{ old('line_1') }}"
                name="line_1"
                class="input @if(count($errors->get('line_1'))) border-red @endif"
                id="line_1"
                type="text"
                placeholder="Where are we going?">
        @if(count($errors->get('line_1')))
            @foreach($errors->get('line_1') as $message)
                <p class="text-red text-xs italic">{{ $message }}</p>
            @endforeach
        @endif
    </div>
    <div class="w-full px-3 mb-6 md:mb-0">
        <label class="block text-grey-darker text-sm font-bold mb-2" for="line_2">
            Suite/Apt
        </label>
        <input  value="{{ old('line_2') }}"
                name="line_2"
                class="input @if(count($errors->get('line_2'))) border-red @endif"
                id="line_2"
                type="text"
                placeholder="Your Suite/Apt Here...">
        @if(count($errors->get('line_2')))
            @foreach($errors->get('line_2') as $message)
                <p class="text-red text-xs italic">{{ $message }}</p>
            @endforeach
        @endif
    </div>
    <div class="w-full md:w-2/3 px-3 mb-6 md:mb-0">
        <label class="block text-grey-darker text-sm font-bold mb-2" for="city">
            Your City
        </label>
        <input  value="{{ old('city') }}"
                name="city"
                class="input @if(count($errors->get('city'))) border-red @endif"
                id="city"
                type="text"
                placeholder="Your city Here...">
        @if(count($errors->get('city')))
            @foreach($errors->get('city') as $message)
                <p class="text-red text-xs italic">{{ $message }}</p>
            @endforeach
        @endif
    </div>
    <div class="w-full w-1/2 md:w-1/6 px-3 mb-6 md:mb-0">
    <label class="block text-grey-darker text-sm font-bold mb-2" for="state">
        Your state
    </label>
    <input  value="{{ old('state') }}"
            name="state"
            class="input @if(count($errors->get('state'))) border-red @endif"
            id="state"
            type="text"
            placeholder="Your state Here...">
    @if(count($errors->get('state')))
        @foreach($errors->get('state') as $message)
            <p class="text-red text-xs italic">{{ $message }}</p>
        @endforeach
    @endif
    </div>
    <div class="w-full w-1/2 md:w-1/6 px-3 mb-6 md:mb-0">
        <label class="block text-grey-darker text-sm font-bold mb-2" for="zip">
            Your zip
        </label>
        <input  value="{{ old('zip') }}"
                name="zip"
                class="input @if(count($errors->get('zip'))) border-red @endif"
                id="zip"
                type="text"
                placeholder="Your zip Here...">
        @if(count($errors->get('zip')))
            @foreach($errors->get('zip') as $message)
                <p class="text-red text-xs italic">{{ $message }}</p>
            @endforeach
        @endif
    </div>
</div>

@section('scripts')
    <script src="https://cdn.jsdelivr.net/npm/places.js@1.14.0"></script>
    <script>
        (function() {
            var placesAutocomplete = places({
//                appId: 'plVWWLQODWJN',
                apiKey: 'efda34499c5c9a6484b809eb95ce131b',
                container: document.querySelector('#line_1'),
                templates: {
                    value: function(suggestion) {
                        return suggestion.name;
                    }
                }
            }).configure({
                type: 'address'
            });
            placesAutocomplete.on('change', function resultSelected(e) {
                document.querySelector('#state').value = e.suggestion.administrative || '';
                document.querySelector('#city').value = e.suggestion.city || '';
                document.querySelector('#zip').value = e.suggestion.postcode || '';
            });
        })();
    </script>

    <script>
        new flatpickr("#depart_at", {
            disableMobile: false,
            enableTime: true,
            dateFormat: 'Z',
            altInput: true,
            altFormat: 'Y-m-d h:i K',
//            defaultDate: new Date(),
        });

        new flatpickr("#return_at", {
            disableMobile: false,
            enableTime: true,
            dateFormat: 'Z',
            altInput: true,
            altFormat: 'Y-m-d h:i K',
//            defaultDate: new Date(),
        });
    </script>
@endsection